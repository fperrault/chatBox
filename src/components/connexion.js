import React from 'react';

class Connexion extends React.Component {
    goToChat = (event) => {
        event.preventDefault();//sinon actualisation
        const pseudo = this.pseudoInput.value;//Value sinon renvoie tout l'input

        //changer l'url
        this.context.router.transitionTo(`/pseudo/${pseudo}`);
    };

    render () {
        return (
            <div className="connexionBox">
                <form className="connexion" onSubmit={(event) => this.goToChat(event)}>
                    <input 
                        type="text" 
                        placeholder="Pseudo" 
                        required 
                        ref={(input) => this.pseudoInput = input}
                    />
                    <button type="submit">GO !</button>
                </form>
            </div>
        )
    }

    //appel de reactRouter dans le component
    static contextTypes = {
        router: React.PropTypes.object
    }
}

export default Connexion;