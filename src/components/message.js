import React from 'react';

class Message extends React.Component {
    preRender = (isUser) => {
        if (isUser) {
            return (
                <p className="user-message">
                    {this.props.detailMessage.message}
                </p>
            )
        } else {
            return (
                <p className="not-user-message">
                    <b>{this.props.detailMessage.pseudo}</b> : {this.props.detailMessage.message}
                </p>
            )
        }
    }
    render() {
        return this.preRender(this.props.isUser(this.props.detailMessage.pseudo))
    }

    static propTypes = {
        detailMessage: React.PropTypes.object.isRequired
    }
}

export default Message;