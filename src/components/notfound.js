import React from 'react';

class NotFound extends React.Component {
    goHome = () => {
        this.context.router.transitionTo(`/`);
    }
    render() {
        return(
            <div className="notFound">
                <h2 >Il n'y a rien ici !</h2>
                <button onClick={() => this.goHome()}>Accueil</button>
            </div>
        )
    }

    static contextTypes = {
        router: React.PropTypes.object
    }
}

export default NotFound;