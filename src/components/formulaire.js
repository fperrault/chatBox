import React from 'react';

class Formulaire extends React.Component {

    state = {
        textLength: this.props.messageLength
    }

    createMessage = event => {
        event.preventDefault();//stopper le rechargement dû au submit
        const message = {
            pseudo: this.props.pseudo,
            message: this.message.value
        };
        this.props.addMessage(message);

        //reset
        this.messageForm.reset();
        const lengthProps = this.props.messageLength;
        this.setState({textLength: lengthProps});
    };

    compteur = event => {
        const textLength = this.props.messageLength - this.message.value.length;
        this.setState({textLength: textLength});
    };

    render () {
        return (
            <form 
                className="form" 
                onSubmit={event => this.createMessage(event)}
                ref={input => this.messageForm = input}
            >
                <textarea 
                    required 
                    maxLength={this.state.textLength}
                    ref={input => this.message = input}
                    onChange={event => this.compteur(event)}
                >
                </textarea>
                <div className="info">{this.state.textLength}</div>
                <button type="submit">Envoyer !</button>
            </form>
        )
    }

    static propTypes = {
        addMessage: React.PropTypes.func.isRequired,
        pseudo: React.PropTypes.string.isRequired,
        messageLength: React.PropTypes.number.isRequired
    }
}

export default Formulaire;