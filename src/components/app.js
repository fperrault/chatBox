import React from 'react';
import Formulaire from './formulaire';
import Message from './message';
import database from '../database.js';
//CSS
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import '../css/animation.css';

class App extends React.Component {

    state = {
        messages: {}
    }

    componentWillMount(){ //avant que le component soit monté
        this.ref = database.syncState('/', {  //a la racine
            context: this,
            state: 'messages'
        })
    }

    componentDidUpdate(){//A chaque changement
        //Mettre le scroll en bas 
        this.niveauMessage.scrollTop = this.niveauMessage.scrollHeight;
    }

    addMessage = (message) => {
        //copier le state
        const messages = {...this.state.messages}; //"..." = spread = "récupère tout"
        //ajoute le message avec clé timestamp
        const timestamp = Date.now();
        messages[`message-${timestamp}`] = message;
        //Supprime si trop de messages
        Object.keys(messages).slice(0, -10).map(key => messages[key] = null);
        //MaJ le state
        this.setState({messages: messages})
    };

    isUser = (pseudo) => {
        return pseudo === this.props.params.pseudo;//return true or false
    }

    render () {
        const messages = Object
            .keys(this.state.messages)
            .map(key => <Message 
                            key={key} 
                            detailMessage={this.state.messages[key]}
                            isUser={this.isUser}
                        />
                )//boucle à l'intérieur d'un tab
        ;

        return (
            <div className="box">
                <div>
                    <div className="messages" ref={input => this.niveauMessage = input}>
                        <ReactCSSTransitionGroup
                            component="div"
                            className="message"
                            transitionName="message"
                            transitionEnterTimeout={400}
                            transitionLeaveTimeout={400}
                        > 
                            {messages} 
                        </ReactCSSTransitionGroup>
                    </div>
                    <Formulaire 
                        addMessage={this.addMessage}
                        pseudo={this.props.params.pseudo}
                        messageLength={140}
                    />
                </div>
            </div>
        )
    }

    static propTypes = {
        params: React.PropTypes.object.isRequired
    };
}

export default App;