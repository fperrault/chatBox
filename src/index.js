//REACT
import React from 'react';
import {render} from 'react-dom';
//        Component     if=   else
import {BrowserRouter, Match, Miss} from 'react-router';
//CSS
import './css/index.css';
//COMPONENTS
import Connexion from './components/connexion';
import App from './components/app';
import NotFound from './components/notfound';


const Root = () => {
    //stateLess
    return(
        <BrowserRouter>
            <div>
                <Match exactly pattern="/" component={Connexion}/>
                <Match exactly pattern="/pseudo/:pseudo" component={App}/>
                <Miss component={NotFound}/>
            </div>
        </BrowserRouter>
    )
}


render(
    <Root/>,
    document.getElementById('root')
)